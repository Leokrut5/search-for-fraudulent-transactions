	SELECT 
		t1.transaction_id,
		t3.transaction_id,
		t2.terminal_city,
		t3.terminal_city
	FROM DWH_FACT_TRANSACTIONS t1
	INNER JOIN DWH_DIM_TERMINALS_HIST t2
	ON t1.terminal = t2.terminal_id
	INNER JOIN 
	(
		SELECT 
			t1.transaction_id,
			t2.terminal_city,
			t1.card_num
		FROM DWH_FACT_TRANSACTIONS t1
		INNER JOIN DWH_DIM_TERMINALS_HIST t2
		ON t1.terminal = t2.terminal_id
	) as t3
	ON t1.card_num=t3.card_num
	AND t1.transaction_id <> t3.transaction_id
	AND t2.terminal_city <> t3.terminal_city
	WHERE abs(julianday(t1.transaction_date) - julianday(t3.transaction_date) * 24 ) < 1
	AND t1.oper_result = 'SUCCESS'
	AND t2.deleted_flg = 0 
	AND ? BETWEEN t2.effective_from AND t2.effective_to

def find_scammers_3(data):
		cursor.execute('''
			SELECT
				t2.client_id
			FROM DWH_DIM_TERMINALS_HIST t1
			INNER JOIN (
				SELECT
					t2.client_id,
					t1.terminals
				FROM DWH_FACT_TRANSACTIONS t1 
				INNER JOIN 
				(
					SELECT 
						t1.card_num,
						t2.client_id
					FROM cards t1
					INNER JOIN 
					(
						SELECT 
							t2.client_id,
							t1.account
						FROM accounts t1
						INNER JOIN clients t2
						on t1.client = t2.client_id
					) as t2
					on t1.account = t2.account
				) as t2
				ON t1.card_num = t2.card_num
				WHERE t1.oper_result = 'SUCCESS'
			) as t2
			ON t1.terminal=t2.terminal
			WHERE t1.deleted_flg = 0 
			AND ? BETWEEN t1.effective_from AND t1.effective_to
	''', [data])
	for row in cursor.fetchall():
		print(row)





		def find_scammers_3(data):
	cursor.execute('''
		SELECT 
			t1.transaction_id,
			t3.transaction_id,
			t2.terminal_city,
			t3.terminal_city
		FROM DWH_FACT_TRANSACTIONS t1
		INNER JOIN DWH_DIM_TERMINALS_HIST t2
		ON t1.terminal = t2.terminal_id
		INNER JOIN 
		(
			SELECT
				t2.client_id,
				t2.terminal,
				t2.transaction_id,
				t1.terminal_city,
				t2.transaction_date,
				t2.card_num
			FROM DWH_DIM_TERMINALS_HIST t1
			INNER JOIN 
			(
				SELECT
					t2.client_id,
					t1.terminal,
					t2.card_num,
					t1.transaction_id,
					t1.transaction_date
				FROM DWH_FACT_TRANSACTIONS t1 
				INNER JOIN 
				(
					SELECT 
						t1.card_num,
						t2.client_id
					FROM cards t1
					INNER JOIN 
					(
						SELECT 
							t2.client_id,
							t1.account
						FROM accounts t1
						INNER JOIN clients t2
						ON t1.client = t2.client_id
					) as t2
					ON t1.account = t2.account
				) as t2
				ON t1.card_num = t2.card_num
				WHERE t1.oper_result = 'SUCCESS'
			) as t2
			ON t1.terminal_id=t2.terminal
			WHERE t1.deleted_flg = 0 
			AND ? BETWEEN t1.effective_from AND t1.effective_to
		) as t3
		ON t1.card_num=t3.card_num
		AND t1.transaction_id <> t3.transaction_id
		AND t2.terminal_city <> t3.terminal_city
		WHERE abs(julianday(t1.transaction_date) - julianday(t3.transaction_date) * 24 ) < 1
		AND t1.oper_result = 'SUCCESS'
		AND t2.deleted_flg = 0 
		AND ? BETWEEN t2.effective_from AND t2.effective_to
	''', [data, data])



	cursor.execute('''
		SELECT 
		t1.transaction_id,
		t3.transaction_id,
		t2.terminal_city,
		t3.terminal_city,
		abs(julianday(t1.transaction_date) - julianday(t3.transaction_date)) * 24
	FROM DWH_FACT_TRANSACTIONS t1
	INNER JOIN DWH_DIM_TERMINALS_HIST t2
	ON t1.terminal = t2.terminal_id
	INNER JOIN 
	(
		SELECT 
			t1.transaction_id,
			t2.terminal_city,
			t1.card_num,
			t1.transaction_date
		FROM DWH_FACT_TRANSACTIONS t1
		INNER JOIN DWH_DIM_TERMINALS_HIST t2
		ON t1.terminal = t2.terminal_id
	) as t3
	ON t1.card_num=t3.card_num
	AND t1.transaction_id <> t3.transaction_id
	AND t2.terminal_city <> t3.terminal_city
	WHERE abs(julianday(t1.transaction_date) - julianday(t3.transaction_date)) * 24 < 1
	AND t1.oper_result = 'SUCCESS'
	AND t2.deleted_flg = 0 
	AND ? BETWEEN t2.effective_from AND t2.effective_to

	
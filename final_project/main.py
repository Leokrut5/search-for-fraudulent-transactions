import sqlite3
import pandas as pd
import openpyxl
import os

conn = sqlite3.connect('database.db')

cursor = conn.cursor()

def csv2sql(filepath, table_name):
	df = pd.read_csv(filepath, sep=';', header=0)
	df.to_sql(table_name, conn, if_exists='replace', index=False)

def exel2sql(filepath, table_name):
	df = pd.read_excel(filepath, engine='openpyxl')
	df.to_sql(table_name, conn, if_exists='replace', index=False)

def showTable(table_name):
	cursor.execute('SELECT * FROM ' + table_name)
	for row in cursor.fetchall():
		print(row)

def trigger_function():
	open_sql()

	day = input('Введите день создания загружаемых таблиц в формате DD(01, 02, 03): ')
	data = '2021-03-' + day

	csv2sql('transactions_' + day + '032021.txt', 'DWH_FACT_TRANSACTIONS')
	
	exel2sql('passport_blacklist_' + day + '032021.xlsx', 'DWH_FACT_PASSPORT_BLACKLIST')
	
	# Создание таблицы терминалс в формате SCD2
	exel2sql('terminals_' + day + '032021.xlsx', 'STG_TERMINALS')
	init_hist_tables()
	create_new_tables()
	create_deleted_tables()
	create_updated_tables()
	update_tables(data)
	delete_STG_table()

	# Создание таблицы отчета:
	create_REP_FRAUD()

	# Поиск мошенников и добавление их в таблицу отчета
	find_scammers_1(data)
	find_scammers_2(data)
	find_scammers_3(data)
	showTable('REP_FRAUD')
	# Переименование и перенос отработанных файлов 

	# os.replace("transactions_" + day + "032021.txt", "archive/transactions_" + day + "032021.txt.backup")
	# os.replace("passport_blacklist_" + day + "032021.xlsx", "archive/passport_blacklist_" + day + "032021.xlsx.backup")
	# os.replace("terminals_" + day + "032021.xlsx", "archive/terminals_" + day + "032021.xlsx.backup")
	

def open_sql(filepath='ddl_dml.sql'):
	try:
		with open(filepath, 'r', encoding='utf-8') as f:
			file = f.read()
			cursor.executescript(file)
			cursor.executescript('''
				ALTER TABLE `clients` RENAME TO `STG_CLIENTS`;
				ALTER TABLE 'accounts' RENAME TO 'STG_ACCOUNTS';
				ALTER TABLE 'cards' RENAME TO 'STG_CARDS';
			''')
	except sqlite3.OperationalError:
		print('Файл ddl_dml.sql уже загружен')


def init_hist_tables():
	cursor.execute('''
		CREATE TABLE IF NOT EXISTS DWH_DIM_TERMINALS_HIST(
			terminal_id varchar(128),
			terminal_type varchar(128),
			terminal_city varchar(128),
			terminal_address varchar(128),
			effective_from datetime default (datetime('2021-01-03 00:00:00')),
			effective_to datetime default (datetime('2999-12-31 23:59:59')),
			deleted_flg datetime default 0
		)
	''')

	cursor.execute('''
		CREATE VIEW IF NOT EXISTS v_terminals as
			SELECT 
				terminal_id,
				terminal_type,
				terminal_city,
				terminal_address,
				effective_from,
				effective_to,
				deleted_flg
			FROM DWH_DIM_TERMINALS_HIST
			WHERE current_timestamp BETWEEN effective_from AND effective_to
			AND deleted_flg = 0
	''')

	cursor.execute('''
		CREATE TABLE IF NOT EXISTS DWH_DIM_CARDS_HIST(
			card_num varchar(128), 
			account varchar(128), 
			effective_from datetime default (datetime('2021-01-03 00:00:00')),
			effective_to datetime default (datetime('2999-12-31 23:59:59')),
			deleted_flg datetime default 0
		)
	''')

	cursor.execute('''
		CREATE VIEW IF NOT EXISTS v_cards as
			SELECT 
				card_num,
				account,
				effective_from,
				effective_to,
				deleted_flg
			FROM DWH_DIM_CARDS_HIST
			WHERE current_timestamp BETWEEN effective_from AND effective_to
			AND deleted_flg = 0
	''')

	cursor.execute('''
		CREATE TABLE IF NOT EXISTS DWH_DIM_ACCOUNTS_HIST(
			account varchar(128), 
			valid_to date, 
			client varchar(128),
			effective_from datetime default (datetime('2021-01-03 00:00:00')),
			effective_to datetime default (datetime('2999-12-31 23:59:59')),
			deleted_flg datetime default 0
		)
	''')

	cursor.execute('''
		CREATE VIEW IF NOT EXISTS v_accounts as
			SELECT 
				account, 
				valid_to, 
				client,
				effective_from,
				effective_to,
				deleted_flg
			FROM DWH_DIM_ACCOUNTS_HIST
			WHERE current_timestamp BETWEEN effective_from AND effective_to
			AND deleted_flg = 0
	''')

	cursor.execute('''
		CREATE TABLE IF NOT EXISTS DWH_DIM_CLIENTS_HIST(
			client_id integer, 
			last_name varchar(128), 
			first_name varchar(128), 
			patronymic varchar(128), 
			date_of_birth date, 
			passport_num varchar(128), 
			passport_valid_to date, 
			phone varchar(128),
			effective_from datetime default (datetime('2021-01-03 00:00:00')),
			effective_to datetime default (datetime('2999-12-31 23:59:59')),
			deleted_flg datetime default 0
		)
	''')

	cursor.execute('''
		CREATE VIEW IF NOT EXISTS v_clients as
			SELECT 
				client_id,
				last_name, 
				first_name, 
				patronymic, 
				date_of_birth, 
				passport_num, 
				passport_valid_to, 
				phone,
				effective_from,
				effective_to,
				deleted_flg
			FROM DWH_DIM_CLIENTS_HIST
			WHERE current_timestamp BETWEEN effective_from AND effective_to
			AND deleted_flg = 0
	''')
def create_new_tables():
	cursor.execute('''
		CREATE TABLE IF NOT EXISTS STG_NEW_TERMINALS as
			SELECT 
				t1.terminal_id,
				t1.terminal_type,
				t1.terminal_city,
				t1.terminal_address
		FROM STG_TERMINALS t1
		LEFT JOIN v_terminals t2
		ON t1.terminal_id = t2.terminal_id
		WHERE t2.terminal_id IS null
	''')

	cursor.execute('''
		CREATE TABLE IF NOT EXISTS STG_NEW_CARDS as
			SELECT 
				t1.card_num,
				t1.account
		FROM STG_CARDS t1
		LEFT JOIN v_cards t2
		ON t1.card_num = t2.card_num
		WHERE t2.card_num IS null
	''')

	cursor.execute('''
		CREATE TABLE IF NOT EXISTS STG_NEW_ACCOUNTS as
			SELECT 
				t1.account, 
				t1.valid_to, 
				t1.client
		FROM STG_ACCOUNTS t1
		LEFT JOIN v_accounts t2
		ON t1.account = t2.account
		WHERE t2.account IS null
	''')
	cursor.execute('''
		CREATE TABLE IF NOT EXISTS STG_NEW_CLIENTS as
			SELECT 
				t1.client_id,
				t1.last_name, 
				t1.first_name, 
				t1.patronymic, 
				t1.date_of_birth, 
				t1.passport_num, 
				t1.passport_valid_to, 
				t1.phone
		FROM STG_CLIENTS t1
		LEFT JOIN v_clients t2
		ON t1.client_id = t2.client_id
		WHERE t2.client_id IS null
	''')

def create_deleted_tables():
	cursor.execute('''
		CREATE TABLE if not exists STG_DELETED_TERMINALS as 
		SELECT 
			t1.terminal_id,
			t1.terminal_type,
			t1.terminal_city,
			t1.terminal_address
		FROM v_terminals t1
		LEFT JOIN STG_TERMINALS t2
		on t1.terminal_id = t2.terminal_id
		WHERE t2.terminal_id is null
	''')
	cursor.execute('''
		CREATE TABLE if not exists STG_DELETED_CARDS as 
		SELECT 
			t1.card_num,
			t1.account
		FROM v_cards t1
		LEFT JOIN STG_CARDS t2
		ON t1.card_num = t2.card_num
		WHERE t2.card_num is null
	''')
	cursor.execute('''
		CREATE TABLE if not exists STG_DELETED_ACCOUNTS as 
		SELECT 
			t1.account, 
			t1.valid_to, 
			t1.client
		FROM v_accounts t1
		LEFT JOIN STG_ACCOUNTS t2
		ON t1.account = t2.account
		WHERE t2.account is null
	''')
	cursor.execute('''
		CREATE TABLE if not exists STG_DELETED_CLIENTS as 
		SELECT 
			t1.client_id,
			t1.last_name, 
			t1.first_name, 
			t1.patronymic, 
			t1.date_of_birth, 
			t1.passport_num, 
			t1.passport_valid_to, 
			t1.phone
		FROM v_clients t1
		LEFT JOIN STG_CLIENTS t2
		ON t1.client_id = t2.client_id
		WHERE t2.client_id is null
	''')

def create_updated_tables():
	cursor.execute('''
		CREATE TABLE if not exists STG_UPDATED_TERMINALS as
		SELECT
			t1.terminal_id,
			t1.terminal_type,
			t1.terminal_city,
			t1.terminal_address
		FROM STG_TERMINALS t1
		inner join v_terminals t2
		ON t1.terminal_id = t2.terminal_id
		AND (
			t1.terminal_type <> t2.terminal_type or
			t1.terminal_city <> t2.terminal_city or
			t1.terminal_address <> t2.terminal_address
		)
	''')
	cursor.execute('''
		CREATE TABLE if not exists STG_UPDATED_CARDS as
		SELECT
			t1.card_num,
			t1.account
		FROM STG_CARDS t1
		inner join v_cards t2
		ON t1.card_num = t2.card_num
		AND t1.account <> t2.account
	''')
	cursor.execute('''
		CREATE TABLE if not exists STG_UPDATED_ACCOUNTS as
		SELECT
			t1.account, 
			t1.valid_to, 
			t1.client
		FROM STG_ACCOUNTS t1
		inner join v_accounts t2
		ON t1.account = t2.account
		AND (
			t1.valid_to <> t2.valid_to or
			t1.client <> t2.client
		)
	''')
	cursor.execute('''
		CREATE TABLE if not exists STG_UPDATED_CLIENTS as
		SELECT
			t1.client_id,
			t1.last_name, 
			t1.first_name, 
			t1.patronymic, 
			t1.date_of_birth, 
			t1.passport_num, 
			t1.passport_valid_to, 
			t1.phone
		FROM STG_CLIENTS t1
		inner join v_clients t2
		ON t1.client_id = t2.client_id
		AND (
			t1.client_id <> t2.client_id or
			t1.last_name <> t2.last_name or
			t1.first_name <> t2.first_name or
			t1.patronymic <> t2.patronymic or
			t1.date_of_birth <> t2.date_of_birth or
			t1.passport_num <> t2.passport_num or
			t1.passport_valid_to <> t2.passport_valid_to or
			t1.phone <> t2.phone
		)
	''')

def update_tables(data):
	cursor.execute('''
		INSERT INTO DWH_DIM_TERMINALS_HIST(
			terminal_id,
			terminal_type,
			terminal_city,
			terminal_address,
			effective_from
		) SELECT
			terminal_id,
			terminal_type,
			terminal_city,
			terminal_address,
			?
		FROM STG_NEW_TERMINALS
	''', [data])

	cursor.execute('''
		UPDATE DWH_DIM_TERMINALS_HIST 
		set effective_to = ?
		-- datetime('now', '-1 second')
		WHERE terminal_id in (select terminal_id FROM STG_UPDATED_TERMINALS)
		AND effective_to = datetime('2999-12-31 23:59:59')
	''', [data])

	cursor.execute('''
		INSERT INTO DWH_DIM_TERMINALS_HIST(
			terminal_id,
			terminal_type,
			terminal_city,
			terminal_address
		) SELECT
			terminal_id,
			terminal_type,
			terminal_city,
			terminal_address
		FROM STG_UPDATED_TERMINALS
	''')

	cursor.execute('''
		UPDATE DWH_DIM_TERMINALS_HIST 
		set effective_to = ?
		WHERE terminal_id in (select terminal_id FROM STG_DELETED_TERMINALS)
		AND effective_to = datetime('2999-12-31 23:59:59')
	''', [data])

	cursor.execute('''
		INSERT INTO DWH_DIM_TERMINALS_HIST(
			terminal_id,
			terminal_type,
			terminal_city,
			terminal_address,
			deleted_flg
		) SELECT
			terminal_id,
			terminal_type,
			terminal_city,
			terminal_address,
			1
		FROM STG_DELETED_TERMINALS
	''')

	# CARDS

	cursor.execute('''
		INSERT INTO DWH_DIM_CARDS_HIST(
			card_num,
			account,
			effective_from
		) SELECT
			card_num,
			account,
			?
		FROM STG_NEW_CARDS
	''', [data])

	cursor.execute('''
		UPDATE DWH_DIM_CARDS_HIST 
		SET effective_to = ?
		WHERE card_num IN (SELECT card_num FROM STG_UPDATED_CARDS)
		AND effective_to = datetime('2999-12-31 23:59:59')
	''', [data])

	cursor.execute('''
		INSERT INTO DWH_DIM_CARDS_HIST(
			card_num,
			account
		) SELECT
			card_num,
			account
		FROM STG_UPDATED_CARDS
	''')

	cursor.execute('''
		UPDATE DWH_DIM_CARDS_HIST 
		SET effective_to = ?
		WHERE card_num in (SELECT card_num FROM STG_DELETED_CARDS)
		AND effective_to = datetime('2999-12-31 23:59:59')
	''', [data])

	cursor.execute('''
		INSERT INTO DWH_DIM_CARDS_HIST(
			card_num,
			account,
			deleted_flg
		) SELECT
			card_num,
			account,
			1
		FROM STG_DELETED_CARDS
	''')


	# accounts
	cursor.execute('''
		INSERT INTO DWH_DIM_ACCOUNTS_HIST(
			account, 
			valid_to, 
			client,
			effective_from
		) SELECT
			account, 
			valid_to, 
			client,
			?
		FROM STG_NEW_ACCOUNTS
	''', [data])

	cursor.execute('''
		UPDATE DWH_DIM_ACCOUNTS_HIST 
		SET effective_to = ?
		WHERE account IN (SELECT account FROM STG_UPDATED_ACCOUNTS)
		AND effective_to = datetime('2999-12-31 23:59:59')
	''', [data])

	cursor.execute('''
		INSERT INTO DWH_DIM_ACCOUNTS_HIST(
			account, 
			valid_to, 
			client
		) SELECT
			account, 
			valid_to, 
			client
		FROM STG_UPDATED_ACCOUNTS
	''')

	cursor.execute('''
		UPDATE DWH_DIM_ACCOUNTS_HIST 
		SET effective_to = ?
		WHERE account IN (SELECT account FROM STG_DELETED_ACCOUNTS)
		AND effective_to = datetime('2999-12-31 23:59:59')
	''', [data])

	cursor.execute('''
		INSERT INTO DWH_DIM_ACCOUNTS_HIST(
			account, 
			valid_to, 
			client,
			deleted_flg
		) SELECT
			account, 
			valid_to, 
			client,	
			1
		FROM STG_DELETED_ACCOUNTS
	''')

	cursor.execute('''
		INSERT INTO DWH_DIM_CLIENTS_HIST(
			client_id,
			last_name, 
			first_name, 
			patronymic, 
			date_of_birth, 
			passport_num, 
			passport_valid_to, 
			phone,
			effective_from
		) SELECT
			client_id,
			last_name, 
			first_name, 
			patronymic, 
			date_of_birth, 
			passport_num, 
			passport_valid_to, 
			phone,
			?
		FROM STG_NEW_CLIENTS
	''', [data])

	cursor.execute('''
		UPDATE DWH_DIM_CLIENTS_HIST 
		SET effective_to = ?
		WHERE client_id IN (SELECT client_id FROM STG_UPDATED_CLIENTS)
		AND effective_to = datetime('2999-12-31 23:59:59')
	''', [data])

	cursor.execute('''
		INSERT INTO DWH_DIM_CLIENTS_HIST(
			client_id,
			last_name, 
			first_name, 
			patronymic, 
			date_of_birth, 
			passport_num, 
			passport_valid_to, 
			phone
		) SELECT
			client_id,
			last_name, 
			first_name, 
			patronymic, 
			date_of_birth, 
			passport_num, 
			passport_valid_to, 
			phone
		FROM STG_UPDATED_CLIENTS
	''')

	cursor.execute('''
		UPDATE DWH_DIM_CLIENTS_HIST 
		SET effective_to = ?
		WHERE client_id IN (SELECT client_id FROM STG_DELETED_CLIENTS)
		AND effective_to = datetime('2999-12-31 23:59:59')
	''', [data])

	cursor.execute('''
		INSERT INTO DWH_DIM_CLIENTS_HIST(
			client_id,
			last_name, 
			first_name, 
			patronymic, 
			date_of_birth, 
			passport_num, 
			passport_valid_to, 
			phone,
			deleted_flg
		) SELECT
			client_id,
			last_name, 
			first_name, 
			patronymic, 
			date_of_birth, 
			passport_num, 
			passport_valid_to, 
			phone,
			1
		FROM STG_DELETED_CLIENTS
	''')
	conn.commit()

def delete_STG_table():
	cursor.execute('DROP TABLE if exists STG_TERMINALS')
	cursor.execute('DROP TABLE if exists STG_NEW_TERMINALS')
	cursor.execute('DROP TABLE if exists STG_UPDATED_TERMINALS')
	cursor.execute('DROP TABLE if exists STG_DELETED_TERMINALS')

	cursor.execute('DROP TABLE if exists STG_CARDS')
	cursor.execute('DROP TABLE if exists STG_NEW_CARDS')
	cursor.execute('DROP TABLE if exists STG_UPDATED_CARDS')
	cursor.execute('DROP TABLE if exists STG_DELETED_CARDS')

	cursor.execute('DROP TABLE if exists STG_ACCOUNTS')
	cursor.execute('DROP TABLE if exists STG_NEW_ACCOUNTS')
	cursor.execute('DROP TABLE if exists STG_UPDATED_ACCOUNTS')
	cursor.execute('DROP TABLE if exists STG_DELETED_ACCOUNTS')

	cursor.execute('DROP TABLE if exists STG_CLIENTS')
	cursor.execute('DROP TABLE if exists STG_NEW_CLIENTS')
	cursor.execute('DROP TABLE if exists STG_UPDATED_CLIENTS')
	cursor.execute('DROP TABLE if exists STG_DELETED_CLIENTS')

def create_REP_FRAUD():
	cursor.execute('''
		CREATE TABLE IF NOT EXISTS REP_FRAUD(
			event_dt datetime,
			passport varchar(128),
			fio varchar(128),
			phone varchar(128),
			event_type varchar(128),
			report_dt datetime default current_timestamp
		)
	''')



def find_scammers_2(data):
	# Поиск скамеров с недействительным договором
	cursor.execute('''
		INSERT INTO REP_FRAUD(
			event_dt,
			passport,
			fio,
			phone,
			event_type
		) 
		SELECT
			t2.transaction_date,
			t1.passport_num,
			t1.last_name || ' ' || t1.first_name || ' ' || t1.patronymic,
			t1.phone,
			'2' as event_type
		FROM DWH_DIM_CLIENTS_HIST t1
		INNER JOIN 
		(
			SELECT
				t2.client_id,
				t1.transaction_date
			FROM DWH_FACT_TRANSACTIONS t1 
			INNER JOIN 
			(
				SELECT 
					t1.card_num,
					t2.client_id,
					t2.valid_to
				FROM DWH_DIM_CARDS_HIST t1
				INNER JOIN 
				(
					SELECT 
						t2.client_id,
						t1.account,
						datetime(t1.valid_to, '+1 day') as valid_to
					FROM DWH_DIM_ACCOUNTS_HIST t1
					INNER JOIN DWH_DIM_CLIENTS_HIST t2
					ON t1.client = t2.client_id
					WHERE t1.deleted_flg = 0 
					AND ? BETWEEN t1.effective_from AND t1.effective_to
					AND t2.deleted_flg = 0 
					AND ? BETWEEN t2.effective_from AND t2.effective_to
				) as t2
				ON t1.account = t2.account
				WHERE t1.deleted_flg = 0 
				AND ? BETWEEN t1.effective_from AND t1.effective_to
			) as t2
			ON t1.card_num = t2.card_num
			WHERE t1.oper_result = 'SUCCESS'
			AND t1.transaction_date BETWEEN t2.valid_to AND '2999-12-31 23:59:59' 
		) as t2
		ON t1.client_id = t2.client_id
		WHERE t1.deleted_flg = 0 
		AND ? BETWEEN t1.effective_from AND t1.effective_to
	''', [data, data, data, data])
	conn.commit()


def find_scammers_1(data):
	# Поиск скамеров с недействительным/заблокированным паспортом
	cursor.execute('''
		INSERT INTO REP_FRAUD(
			event_dt,
			passport,
			fio,
			phone,
			event_type
		)
		SELECT
			t2.transaction_date,
			t1.passport_num,
			t1.last_name || ' ' || t1.first_name || ' ' || t1.patronymic,
			t1.phone,
			'1' as event_type
		FROM DWH_DIM_CLIENTS_HIST t1
		INNER JOIN 
		(
			SELECT
				t2.client_id,
				t1.transaction_date
			FROM DWH_FACT_TRANSACTIONS t1 
			INNER JOIN 
			(
				SELECT 
					t1.card_num,
					t2.client_id,
					t2.passport_valid_to,
					t2.passport_num
				FROM DWH_DIM_CARDS_HIST t1
				INNER JOIN 
				(
					SELECT 
						t2.client_id,
						t1.account,
						datetime(t2.passport_valid_to, '+1 day') as passport_valid_to,
						t2.passport_num
					FROM DWH_DIM_ACCOUNTS_HIST t1
					INNER JOIN DWH_DIM_CLIENTS_HIST t2
					ON t1.client = t2.client_id
					WHERE t1.deleted_flg = 0 
					AND ? BETWEEN t1.effective_from AND t1.effective_to
					AND t2.deleted_flg = 0 
					AND ? BETWEEN t2.effective_from AND t2.effective_to
				) as t2
				ON t1.account = t2.account
				WHERE t1.deleted_flg = 0 
				AND ? BETWEEN t1.effective_from AND t1.effective_to
			) as t2
			ON t1.card_num = t2.card_num
			LEFT JOIN DWH_FACT_PASSPORT_BLACKLIST t3
			ON t2.passport_num = t3.passport
			WHERE t1.oper_result = 'SUCCESS'
			AND (
				t1.transaction_date BETWEEN t2.passport_valid_to AND '2999-12-31 23:59:59'
				OR  (
					t1.transaction_date BETWEEN t3.date AND '2999-12-31 23:59:59' 
					AND t2.passport_num IN (SELECT passport FROM DWH_FACT_PASSPORT_BLACKLIST)
					)
				)
		) AS t2
		ON t1.client_id = t2.client_id
		WHERE t1.deleted_flg = 0 
		AND ? BETWEEN t1.effective_from AND t1.effective_to
	''', [data, data, data, data])

	conn.commit()
	# for row in cursor.fetchall():
	# 	print(row)



def find_scammers_3(data):
	# Совершение операций в разных городах в течение одного часа
	cursor.execute('''
		INSERT INTO REP_FRAUD(
			event_dt,
			passport,
			fio,
			phone,
			event_type
		)
		SELECT
			t2.transaction_date,
			t1.passport_num,
			t1.last_name || ' ' || t1.first_name || ' ' || t1.patronymic,
			t1.phone,
			'3' as event_type
		FROM DWH_DIM_CLIENTS_HIST t1
		INNER JOIN
		(
			SELECT DISTINCT
				t1.transaction_date,
				t3.client_id
			FROM DWH_FACT_TRANSACTIONS t1
			INNER JOIN DWH_DIM_TERMINALS_HIST t2
			ON t1.terminal = t2.terminal_id
			INNER JOIN
			(
				SELECT
					t1.account,
					t2.card_num
				FROM DWH_DIM_ACCOUNTS_HIST t1
				INNER JOIN DWH_DIM_CARDS_HIST t2
				ON t1.account = t2.account
				WHERE t1.deleted_flg = 0 
				AND ? BETWEEN t1.effective_from AND t1.effective_to
				AND t2.deleted_flg = 0 
				AND ? BETWEEN t2.effective_from AND t2.effective_to
			) as t4
			ON t1.card_num = t4.card_num 
			INNER JOIN 
			(
				SELECT
					t2.client_id,
					t2.terminal,
					t2.transaction_id,
					t1.terminal_city,
					t2.transaction_date,
					t2.card_num,
					t2.account
				FROM DWH_DIM_TERMINALS_HIST t1
				INNER JOIN 
				(
					SELECT
						t2.client_id,
						t1.terminal,
						t2.card_num,
						t1.transaction_id,
						t1.transaction_date,
						t2.account
					FROM DWH_FACT_TRANSACTIONS t1 
					INNER JOIN 
					(
						SELECT 
							t1.card_num,
							t2.client_id,
							t2.account
						FROM DWH_DIM_CARDS_HIST t1
						INNER JOIN 
						(
							SELECT 
								t2.client_id,
								t1.account
							FROM DWH_DIM_ACCOUNTS_HIST t1
							INNER JOIN DWH_DIM_CLIENTS_HIST t2
							ON t1.client = t2.client_id
							WHERE t1.deleted_flg = 0 
							AND ? BETWEEN t1.effective_from AND t1.effective_to
							AND t2.deleted_flg = 0 
							AND ? BETWEEN t2.effective_from AND t2.effective_to
						) as t2
						ON t1.account = t2.account
						WHERE t1.deleted_flg = 0 
						AND ? BETWEEN t1.effective_from AND t1.effective_to
					) as t2
					ON t1.card_num = t2.card_num
					WHERE t1.oper_result = 'SUCCESS'
				) as t2
				ON t1.terminal_id=t2.terminal
				WHERE t1.deleted_flg = 0 
				AND ? BETWEEN t1.effective_from AND t1.effective_to
			) as t3
			ON t4.account = t3.account
			AND t1.transaction_id <> t3.transaction_id
			AND t2.terminal_city <> t3.terminal_city
			WHERE abs(julianday(t1.transaction_date) - julianday(t3.transaction_date)) * 24 < 1
			AND t1.oper_result = 'SUCCESS'
			AND t2.deleted_flg = 0 
			AND ? BETWEEN t2.effective_from AND t2.effective_to
		) as t2
		ON t1.client_id = t2.client_id
		WHERE t1.deleted_flg = 0 
		AND ? BETWEEN t1.effective_from AND t1.effective_to
	''', [data, data, data, data, data, data, data, data])
	conn.commit()




# СОЗДАТЬ ФУНКЦИЮ С ДОБАВЛЕНИЕМ ВСЕХ ТАБЛИЦ И ФАЙЛОВ РАЗОМ
trigger_function()



# print('_-'*20)
# print('STG_NEW_TERMINALS')
# print('_-'*20)
# showTable('STG_NEW_TERMINALS')

# print('_-'*20)
# print('STG_DELETED_TERMINALS')
# print('_-'*20)
# showTable('STG_DELETED_TERMINALS')

# print('_-'*20)
# print('STG_UPDATED_TERMINALS')
# print('_-'*20) 
# showTable('STG_UPDATED_TERMINALS')

# delete_STG_table()

# open_sql()
# showTable('DWH_DIM_TERMINALS_HIST')
# showTable('DWH_FACT_TRANSACTIONS')
